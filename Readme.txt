 Eu iniciei o projeto dentro da pasta public, como solicitado. Criei, ent�o, um react-app l� dentro com o nome de "web". A minha ideia era criar um estilo mais simples, pois sabia que ia ter desafio na parte l�gica, ent�o optei por criar um estilo mais globalizado no arquivo App.css, deixando para as folhas .module.css alguns ajustes pontuais e o media query. 

Sobre o projeto, decidi separar em componentes, tais quais: Menu, onde decidi colocar um menu lateral e o menu principal com estilos diferentes, fazendo a distin��o com um tern�rio no index; Layout, onde est�o os arquivos footer, header, search (infelizmente n�o consegui fazer ser totalmente funcional, quis deixar ele com fetch para mostrar que tamb�m sei usar) e head, que eu criei para ter uma atualiza��o reativa do title da p�gina; Home, onde fica a p�gina principal; Contact, onde coloquei a p�gina de contato, e; Category. onde est� a p�gina que chama os produtos da API. 

J� sobre a API, eu tive que fazer algumas pequenas mudan�as. A primeira: em cada folha eu tive que colocar o nome da categoria para puxar o nome reativamente como no layout. A outra mudan�a: eu tive que colocar a pasta �media� com as imagens dos itens dentro da pasta public do meu projeto, para que eu conseguisse ter acesso ao caminho da imagem. 

No category eu decidi usar o axios para consumir a API e trazer  as 3 listas de itens na mesma p�gina de uma forma reativa tamb�m. Usei nesse projeto normalize,  react-router, fetch e axios. 

Para rodar o meu projeto � necess�rio dar o npm start na api e depois entrar na pasta web, que est� em public, e rodar npm start l� dentro.
