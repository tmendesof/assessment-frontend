import React from "react";
import styles from "./contato.module.css";
import Head from "../layout/Head";

const ContactComponents = () => {
  return (
    <>
      <section className={styles.contato + " animeFall"}>
        <Head title="webJump | Contatos" description="" />
        <h1 className={styles.title}>Nossos Contatos:</h1>

        <ul>
          <li>thiago@webjump.com.br</li>
          <li>(11) 99999-9999 </li>
          <li>@WebJump!</li>
        </ul>
      </section>
      <img className={styles.img} src="wall_back.jpg" />
    </>
  );
};

export default ContactComponents;
