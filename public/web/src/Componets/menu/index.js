import { useEffect, useState } from "react";
import axios from "axios";
import { NavLink } from "react-router-dom";

const MenuComponent = ({ sidebar }) => {
  const [items, setItems] = useState([]);

  const getCategoryList = async () => {
    const response = await axios.get(
      `http://localhost:8888/api/V1/categories/list`
    );
    const { items } = response.data;
    setItems(items);
  };

  useEffect(() => {
    getCategoryList();
  }, []);

  return (
    <>
      <div className={sidebar ? "menu-left" : "menu-up"}>
        <ul>
          <li>
            <NavLink to="/" end>
              Home
            </NavLink>
          </li>
          {items.length > 0 &&
            items.map((item) => (
              <li key={item.id}>
                <NavLink to={`/${item.id}`}>{item.name}</NavLink>
              </li>
            ))}
          <li>
            <NavLink to="contatos">Contato</NavLink>
          </li>
        </ul>
      </div>
    </>
  );
};

export default MenuComponent;
