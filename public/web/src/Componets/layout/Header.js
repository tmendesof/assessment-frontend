import React from "react";
import MenuComponent from "../menu";
import styles from "./Header.module.css";
import SearchForm from "./SearchForm";

const Header = () => {
  return (
    <header>
      <div>
        <nav>
          <div className={styles.topo}>
            <a className={styles.login} href="#">
              Acesse sua conta
            </a>
            <a className={styles.liga}>ou</a>
            <a className={styles.cadastro} href="#">
              cadastre-se
            </a>
          </div>
          <div>
            <img className={styles.logo} src="logo_webjump.jpg"></img>
            <SearchForm />
            <MenuComponent />
          </div>
        </nav>
      </div>
    </header>
  );
};

export default Header;
