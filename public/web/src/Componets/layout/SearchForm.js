import React from "react";
import styles from "./SearchForm.module.css";

const SearchForm = () => {
  const [id, setId] = React.useState("");

  function handleSubmit(event) {
    event.preventDefault();
    fetch(`http://localhost:8888/api/V1/categories/list/`)
      .then((response) => {
        console.log(response);
        return response.json();
      })
      .then((json) => {
        console.log(json);
        return json;
      });
  }

  return (
    <form onSubmit={handleSubmit} className={styles.search}>
      <input
        className={styles.control}
        type="text"
        value={id}
        onChange={({ target }) => setId(target.value)}
      />
      <button className={styles.btn}>BUSCAR</button>
    </form>
  );
};

export default SearchForm;
