import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import styles from "./category.module.css";
import Head from "../layout/Head";
import axios from "axios";

const CategoryComponent = () => {
  const { categoryId } = useParams();
  const [items, setItems] = useState([]);
  const [title, setTitle] = useState("");

  const getCategory = async (categoryId) => {
    const response = await axios.get(
      `http://localhost:8888/api/V1/categories/${categoryId}`
    );
    const { items, category } = response.data;
    setTitle(category.name);
    setItems(items);
  };

  useEffect(() => {
    getCategory(categoryId);
  }, [categoryId]);

  return (
    <section className={styles.main + " animeFall"}>
      <Head title={"webJump | " + title} description="" />
      <h1 className={styles.title}>{`${title}:`}</h1>
      <div className={styles.cards}>
        {items.length > 0 &&
          items.map((item) => (
            <div className={styles.items} key={item.id}>
              <img className={styles.img} src={`${item.image}`} />
              <p className={styles.description}>{item.name}</p>
              <h3 className={styles.price}>
                {item.price.toLocaleString("pt-br", {
                  style: "currency",
                  currency: "BRL",
                })}
              </h3>
              <button className={styles.btn}>Comprar</button>
            </div>
          ))}
      </div>
    </section>
  );
};

export default CategoryComponent;
