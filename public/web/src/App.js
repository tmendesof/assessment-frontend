import "./App.css";
import "normalize.css";
import Header from "./Componets/layout/Header";
import Footer from "./Componets/layout/Footer";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import HomeComponent from "./Componets/home";
import CategoryComponent from "./Componets/category";
import ContactComponents from "./Componets/contact";
import MenuComponent from "./Componets/menu";

function App() {
  return (
    <div className="container">
      <BrowserRouter>
        <Header />
        <main className="container__main">
          <aside className="container__left">
            <MenuComponent sidebar />
          </aside>
          <Routes>
            <Route path="/" element={<HomeComponent />} />
            <Route path="/:categoryId" element={<CategoryComponent />} />
            <Route path="contatos" element={<ContactComponents />} />
          </Routes>
        </main>
        <Footer />
      </BrowserRouter>
    </div>
  );
}

export default App;
